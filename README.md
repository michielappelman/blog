# My Personal Page and Blog

This repository contains the resources for building <https://appelman.se>.

## License

This repository is licensed under the MIT license, except for the content of
the following directories:

- `themes`: licensed under their respective license(s)
- `content`: licensed under [CC BY-SA-4](https://creativecommons.org/licenses/by-sa/4.0/)
- `static`: licensed under [CC BY-SA-4](https://creativecommons.org/licenses/by-sa/4.0/)

