+++
title = "About Me"
slug = "about"
aliases = ["about-me", "contact"]
+++

Hi! I'm Michiel 👋 My official name has a *very* Dutch pronunciation, so feel free to call me Michael or Mike. 

## 🚀 Code

When I get the chance, I like to slap together Go/Python/Shell code for automating my home and other types of fun. I release my own code on [Codeberg](https://codeberg.org/michielappelman), but you can also find me on [GitHub](https://github.com/michielappelman) and [GitLab](https://gitlab.com/michielappelman/), primarily for (professional) contributions and forking other people's code.

## 👨‍💻 Work

I currently work at [Cloudflare](https://www.cloudflare.com) as a pre-sales Solutions Engineer. I thoroughly enjoy learning about new technologies, products, and features, getting to the core of what they will enable you to do, and then translating that into valuable solutions to customers' challenges.

You may also remember me from any of my previous positions:

- Technical Solutions Architect on 5G xHaul @  [Cisco](https://www.cisco.com/) 
- Systems Engineer for SP Market @  [Cisco](https://www.cisco.com/) 
- Network Engineer @  [AMS-IX](https://ams-ix.net/) 

Visit [my LinkedIn profile](https://www.linkedin.com/in/michielappelman/) or reach out directly to receive an up-to-date CV, if you are interested in learning more.

## 📚 Education

- **Systems & Network Engineering MSc** @  [OS3 / UvA](https://www.os3.nl/). Research Project and Final Thesis: *[Architecture of Dynamic VPNs in OpenFlow](/RP2-Appelman-DVPNs_in_OpenFlow.pdf)* at UvA / TNO. 
- **Network Infrastructure Design BASc** @  [Hogeschool Zuyd](http://www.zuyd.nl/) 

## 🕹 Spare time

Apart from toying around with code and any new tech, I enjoy:

- Reading (find me on [GoodReads](https://www.goodreads.com/user/show/76558392-michiel-appelman)!)
- Watching anime or more mainstream series
- Dusting off my foldable racing chair and putting in some laps in Gran Turismo
- Writing: contributing to documentation and here on this blog
- Running (or at least I *used to*)

