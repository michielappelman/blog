+++ 
title = "USB to Serial on Yosemite"
slug = "usb-to-serial-on-yosemite"
categories = ["Tech"]
date = 2015-03-16T11:27:45+01:00
images = ["images/blog-twitter-card.png"]
+++

{{< notice warning >}} This is an older post that I imported from my previous blog. I have not validated whether the procedure below is still relevant for Yosemite, but it most certainly will not be for newer macOS releases! {{< /notice >}}

There have been numerous blog posts written on getting USB to Serial adapters working under Mac OS X — and specifically Yosemite. However, none of them gave me the absolute answer on how to get it working perfectly. And when a colleague of mine asked me to help him get his working on his Mac a couple of weeks ago, it seemed all was lost for him as well.

We followed the instructions on a couple of different web pages without any success and finally we just copied the driver I had running to his Mac and everything was working all of a sudden! Magic.

The driver I'm using took me a while to get right as well when I first started using a Mac some years ago. But since then it hadn't failed me. Until of course Yosemite came along and required drivers to be signed:

```
com.apple.kextd[19]: ERROR: invalid signature for com.prolific.driver.PL2303, will not load
```

Luckily this was easily solved by running the following command and rebooting:

```
sudo nvram boot-args="kext-dev-mode=1"
```

## The Correct Driver
Basically, there are two drivers available on the Internet for the Prolific USB to Serial chipset. The first is the Open Source version available on [GitHub](https://github.com/mpepping/osx-pl2303/) and the second is from a website called [Plugable](http://plugable.com/drivers/prolific/). As far as I can see the results are mixed for any of the adapters, but I'm using the latter.

When I first downloaded and installed this driver, it didn't work until I made some changes in the `Info.plist` file included in the driver. To do so, I first unloaded the driver and opened the Info file:

```
sudo kextunload /System/Library/Extensions/ProlificUsbSerial.kext
sudo vi /System/Library/Extensions/ProlificUsbSerial.kext/Contents/Info.plist
```

In it you will find the `067B_2304` key which includes a certain `idProduct` and `idVendor`. These settings are the ones I had to change to reflect the following values:

```xml
    <key>067B_2304</key>                                                       
    <dict>                                                                     
        <key>CFBundleIdentifier</key>                                          
        <string>com.prolific.driver.PL2303</string>                            
        <key>IOClass</key>                                                     
        <string>com_prolific_driver_PL2303</string>                            
        <key>IOProviderClass</key>                                             
        <string>IOUSBInterface</string>                                        
        <key>bConfigurationValue</key>                                         
        <integer>1</integer>                                                   
        <key>bInterfaceNumber</key>                                            
        <integer>0</integer>                                                   
        <key>idProduct</key>                                                   
        <integer>8296</integer>                                                
        <key>idVendor</key>                                                    
        <integer>24969</integer>                                               
    </dict>  
```

And when finished you can reload the kext-file: `sudo kextload /System/Library/Extensions/ProlificUsbSerial.kext`

### Tested Devices
The known-working USB to Serial adapters for this driver are the following: 

 * Sitecom USB to Serial Cable 0,6m [CN-104](http://www.sitecomlearningcentre.com/products/cn-104v4002/usb-to-serial-cable-0-6m)
 * Trendnet USB to Serial Converter [TU-S9](http://www.trendnet.com/products/proddetail.asp?prod=150_TU-S9) (Version v1.xR)

Please let me know [on Twitter](https://twitter.com/michielappelman) if it works for your device as well, or if you have any other questions.

