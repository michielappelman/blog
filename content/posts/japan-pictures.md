+++ 
title = "Pictures of Japan"
slug = "japan-pictures"
description = "Small selection of pictures taken during our trip to Japan"
categories = ["Other"]
date = 2015-05-31T16:27:45+01:00
images = ["images/blog-twitter-card.png"]
+++

{{< notice info >}} This is an older post that I imported from my previous blog. {{< /notice >}}

So today I've finally finished sorting, selecting and editing the pictures I took during my vacation in Japan. Me and my girlfriend were there for three weeks during the 2015 Sakura season. 

You can view the full album on [my Flickr page](https://www.flickr.com/photos/michielappelman/sets/72157653789506535), but below you'll find some of my favorites.

---

### Business District

In Tokyo we stayed in a nice hotel in the middle of a busy business area. One of those neighbourhoods where the evenings during the week are more exciting than the weekends. Every restaurant is full of hard working [salarymen](http://en.wikipedia.org/wiki/Salaryman) letting of some steam.

<a href="https://www.flickr.com/photos/michielappelman/18316300461" title="8 by Michiel Appelman, on Flickr"><img src="https://c4.staticflickr.com/8/7756/18316300461_041ebc922b_h.jpg" alt="8"></a>

---

### Early bloom

Walking around Tokyo we stumbled across some cherry blossom trees that were already in full bloom, but most of them were still very much closed. This particular tree didn't seem to have made up its mind and had some flowers that were already opening. Overall the *sakura* season started a bit early but as usual was very unpredictable. Luckily for us we caught the whole show during our stay.

<a href="https://www.flickr.com/photos/michielappelman/18314923955" title="54 by Michiel Appelman, on Flickr"><img src="https://c2.staticflickr.com/8/7757/18314923955_a094cfc1a8_h.jpg" alt="54"></a>

---

### Chillin'

Near Hiroshima we visited one of the biggest *torii* in Japan just of the Miyajima island. Here the deer have grown accustomed to the steady flow of tourists and they **will** attack for food. This one seemed pretty chilled out, though. Probably it already stole a big bag of sweets from an unsuspecting *gaijin*.

<a href="https://www.flickr.com/photos/michielappelman/18311029552" title="71 by Michiel Appelman, on Flickr"><img src="https://c1.staticflickr.com/9/8851/18311029552_176bdc669e_h.jpg" alt="71"></a>

---

### Japanese Jazz

One of the most fun evenings in Japan was hanging out in this small Jazz bar situated along the popular Pontochō alley. These guys were awesome and the lady behind the bar made us feel right at home. Definitely recommended: the Stardust Club.

<a href="https://www.flickr.com/photos/michielappelman/17694342803" title="91 by Michiel Appelman, on Flickr"><img src="https://c2.staticflickr.com/8/7776/17694342803_925e1e7d45_h.jpg" alt="91"></a>

---

### Climbing

South of Kyoto on the Kii peninsula is an old trail called Daimon-zaka. It leads up on a big old mountain towards a quiet temple (especially compared to the tourist craziness in Kyoto) and is lined with trees that are hundreds of years old. It's crazy to think old pelgrim monks already made the same trip and saw the same trees during the Edo-period. When you reach the top you are greeted by this beautiful view of a pagoda with the *Nachi no taki* waterfall behind it.

<a href="https://www.flickr.com/photos/michielappelman/17694397153" title="128 by Michiel Appelman, on Flickr"><img src="https://c1.staticflickr.com/1/548/17694397153_d079369458_h.jpg" alt="128"></a>

---

### Watch out for bears

This is the old *Nakasendo* trail with a sign pointing towards one of the two post towns where it runs in between. Well, it used to be longer and was used as a route between Kyoto and Tokyo back in the old days, but nowadays this part is the only piece that is left as it was back then. Along the way you find several bells that can be rung to scare off any bears. We didn't see any, so I guess they worked…

<a href="https://www.flickr.com/photos/michielappelman/18127243628" title="151 by Michiel Appelman, on Flickr"><img src="https://c4.staticflickr.com/8/7751/18127243628_4a67b88c56_h.jpg"  alt="151"></a>

---

### Grand Re-opening

Apparently we were in luck because Himeji castle — one of the very view castles that's original — had been re-opened just a couple of weeks before we arrived. Unfortunately for us, Himeji castle is one of the most loved castles in Japan and because *sakura* was in full bloom when we were in the area, half of Japan was also there… The inside of the castle wasn't very spectacular but I did love the outside!

<a href="https://www.flickr.com/photos/michielappelman/18315105795" title="184 by Michiel Appelman, on Flickr"><img src="https://c1.staticflickr.com/9/8870/18315105795_6c84fab907_h.jpg" alt="184"></a>

---

### Birthday View

A couple of days before we left I had my birthday and we booked a beautiful traditional Japanese hotel with a view from the roof on Mt. Fuji. The view was a bit blocked by the clouds, but we were still lucky to see part of it.

<a href="https://www.flickr.com/photos/michielappelman/17692239304" title="197 by Michiel Appelman, on Flickr"><img src="https://c1.staticflickr.com/1/447/17692239304_0643f5e8f5_h.jpg" alt="197"></a>
